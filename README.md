# Overview
This project demonstrates CI process by using **NUnit** to test **C#** project.

### Project structure
https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-nunit
```
> dotnet ./PrimeService/bin/Debug/netcoreapp3.1/PrimeService.dll
Hello
```

### Testing
https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-test
```
> dotnet test ./PrimeServiceTests 
Test run for /Users/duy/RiderProjects/NUnitTesting/PrimeServiceTests/bin/Debug/netcoreapp3.1/PrimeServiceTests.dll(.NETCoreApp,Version=v3.1)
Microsoft (R) Test Execution Command Line Tool Version 16.7.0
Copyright (c) Microsoft Corporation.  All rights reserved.

Starting test execution, please wait...

A total of 1 test files matched the specified pattern.

Test Run Successful.
Total tests: 4
     Passed: 4
 Total time: 0.7995 Seconds
duy@Duys-MacBook-Pro NUnitTesting % 

```

### GitLab CI
[.gitlab-ci.yml](.gitlab-ci.yml)

![Image](pipeline.png)